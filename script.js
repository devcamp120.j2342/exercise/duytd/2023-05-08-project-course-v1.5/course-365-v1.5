//REGION 1
//REGION 2
$(document).ready(function(){
    //gán sự kiện load trang
    onPageLoading()
})
//REGION 3
//hàm thực hiện sự kiện load trang
function onPageLoading(){
    //gọi api để lấy danh sách khóa học
    $.ajax({
        url: "https://630890e4722029d9ddd245bc.mockapi.io/api/v1/courses",
        type: "GET",
        dataType: "json",
        success: function(response){
            console.log(response);
            filterListOfCourse(response);
            filterListCourseTrending(response);
        },
        error: function(err){
            console.log(err.status);
        }
    })
}
//REGION 4
//hàm lọc danh sách khóa học Trending
function filterListCourseTrending(paramRes){
    var vTrendingCourse = paramRes.filter(function(paramRes){
        return paramRes.isTrending === true;
    });
    showTrendingCourse(vTrendingCourse);
}
// hàm hiển thị danh sách course trending
function showTrendingCourse(paramTrending){
    var vTrendingCourse = $("#div-trending");
    //chạy vòng lập for
    for (let bI = 0; bI < paramTrending.length; bI++) {
        if(bI < 4){
            console.log(paramTrending[bI].courseName);
            var vColDiv = $("<div>").addClass("col-sm-3");
            var vCardDiv = $("<div>").addClass("card mb-3");
            var vImg = $("<img>").addClass("card-img-top").attr("src", paramTrending[bI].coverImage).attr("alt", "Card image cap");
            var vCardBodyDiv = $("<div>").addClass("card-body");
            var vTitleH5 = $("<h5>").addClass("card-title text-primary text-truncate").text(paramTrending[bI].courseName);
            var vDurationP = $("<p>").addClass("card-text").html(`<i class="far fa-clock"></i> ${paramTrending[bI].duration} ${paramTrending[bI].level}`);
            var vPriceP = $("<p>").addClass("card-text").html(`<strong>$${paramTrending[bI].discountPrice}</strong> <del>$${paramTrending[bI].price}</del>`);
            var vCardFooterDiv = $("<div>").addClass("card-footer");
            var vMediaDiv = $("<div>").addClass("media");
            var vTeacherImg = $("<img>").attr("src", paramTrending[bI].teacherPhoto).attr("alt", "Avatar").addClass("mr-3 rounded-circle").attr("width", "64").attr("height", "64");
            var vTeacherNameP = $("<p>").text(paramTrending[bI].teacherName);
            var vBookmarkLink = $("<a>").attr("href", "").addClass("btn");
            var vBookmarkIcon = $("<i>").addClass("far fa-bookmark");
        
            // Gắn các phần tử con vào phần tử cha
            vBookmarkLink.append(vBookmarkIcon);
            vMediaDiv.append(vTeacherImg);
            vMediaDiv.append(vTeacherNameP);
            vMediaDiv.append(vBookmarkLink);
            vCardFooterDiv.append(vMediaDiv);
            vCardBodyDiv.append(vTitleH5);
            vCardBodyDiv.append($("<br>"));
            vCardBodyDiv.append(vDurationP);
            vCardBodyDiv.append(vPriceP);
            vCardDiv.append(vImg);
            vCardDiv.append(vCardBodyDiv);
            vCardDiv.append(vCardFooterDiv);
            vColDiv.append(vCardDiv);
            
            vColDiv.append(vCardDiv);
            // Thêm phần tử cha vào container sử dụng jQuery
            vTrendingCourse.append(vColDiv);
        }
    }
}
// hàm lọc danh sách khóa học Popular
function filterListOfCourse(paramRes){
    var vPopularCourse = paramRes.filter(function(paramRes){
        return paramRes.isPopular === true;
    });
    showCourse(vPopularCourse)
};
// hàm hiển thị danh sách course
function showCourse(paramPopular){
    var vCoursePopular = $("#div-popular");
    var vIndex = 0
    paramPopular.forEach(function(course) {
        console.log(course.courseName);
        if(vIndex < 4){
            var colDiv = $("<div>").addClass("col-sm-3");
            var cardDiv = $("<div>").addClass("card mb-3");
            var img = $("<img>").addClass("card-img-top").attr("src", course.coverImage).attr("alt", "Card image cap");
            var cardBodyDiv = $("<div>").addClass("card-body");
            var titleH5 = $("<h5>").addClass("card-title text-primary text-truncate").text(course.courseName);
            var durationP = $("<p>").addClass("card-text").html(`<i class="far fa-clock"></i> ${course.duration} ${course.level}`);
            var priceP = $("<p>").addClass("card-text").html(`<strong>$${course.discountPrice}</strong> <del>$${course.price}</del>`);
            var cardFooterDiv = $("<div>").addClass("card-footer");
            var mediaDiv = $("<div>").addClass("media");
            var teacherImg = $("<img>").attr("src", course.teacherPhoto).attr("alt", "Avatar").addClass("mr-3 rounded-circle").attr("width", "64").attr("height", "64");
            var teacherNameP = $("<p>").text(course.teacherName);
            var bookmarkLink = $("<a>").attr("href", "").addClass("btn");
            var bookmarkIcon = $("<i>").addClass("far fa-bookmark");

            // Gắn các phần tử con vào phần tử cha
            bookmarkLink.append(bookmarkIcon);
            mediaDiv.append(teacherImg);
            mediaDiv.append(teacherNameP);
            mediaDiv.append(bookmarkLink);
            cardFooterDiv.append(mediaDiv);
            cardBodyDiv.append(titleH5);
            cardBodyDiv.append($("<br>"));
            cardBodyDiv.append(durationP);
            cardBodyDiv.append(priceP);
            cardDiv.append(img);
            cardDiv.append(cardBodyDiv);
            cardDiv.append(cardFooterDiv);
            colDiv.append(cardDiv);
            
            colDiv.append(cardDiv);
            // Thêm phần tử cha vào container sử dụng jQuery
            vCoursePopular.append(colDiv);

            vIndex++;
        }
        
    });
};

